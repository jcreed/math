\documentclass{article}
\input{macros}

% \usepackage{fullpage}

\begin{document}
\title{Tethered Diamonds}
\author{Jason Reed}
\maketitle
\def\rposs{\mathop{\mathrm{poss}}\nolimits}
\def\rrdy{\mathop{\mathrm{rdy}}\nolimits}
\def\lol{\multimap}

\section{Syntax}
$$\begin{tabular}{rl@{\ }l@{\ \ }l}
Worlds&$p,q$&$::=$&$ \alpha\celse\cdots$\\
Right Judgments&$J$&$::=$&$ A \celse (A \rposs^{\ge q}) \celse (A \rrdy^{\ge q})$\\
Contexts &$\Gamma$&$::=$&$ \cdot \celse \Gamma, A[p]\celse \Gamma, A[\ge p] \celse \Gamma, p \ge q $\\
Propositions &$A$&$::=$&$ \square A \celse \Diamond A \celse A \lol A \celse a\celse \cdots $\\
\end{tabular}$$
The general form of the sequent is $\Gamma \prov J[p]$. The judgment $\rrdy$ stands for `ready'. It is an intermediate stage of the possibility judgment
where one left rule has been applied and it is licensed to go back to truth even in the absence of `monadic reflexivity' (rule $jR[T]$). But it is more useful than
 plain truth: in the presence of `monadic transitivity' (rule $\Diamond L[4]$) it can cycle back to $\rrdy$ under a further use of the left rule.
\section{Rules}
First some boring rules to establish what tethering means:
$$\begin{prooftree}
\using init \justifies
\Gamma, a[p] \prov a[p]
\end{prooftree}
\qquad
\begin{prooftree}
\using init \ge \justifies
\Gamma, p \ge q \prov p \ge q
\end{prooftree}$$

$$\begin{prooftree}
\Gamma, A[p] \prov B[p]
\using \lol R \justifies
\Gamma \prov A \lol B[p]
\end{prooftree}
\qquad
\begin{prooftree}
\Gamma \prov A[p] \qquad \Delta, B[p] \prov J[p]
\using \lol L \justifies
\Gamma, \Delta, A \lol B[p] \prov J[p]
\end{prooftree}$$

\noindent Here's the rules for $\square$, and for the `validity' judgment on the left
$$\begin{prooftree}
\Gamma, \alpha \ge p \prov A [\alpha]
\using \square R\justifies
\Gamma \prov \square A [p]
\end{prooftree}
\qquad
\begin{prooftree}
\Gamma, A[\ge p] \prov J[p]
\using \square L\justifies
\Gamma, \square A [p] \prov J[p]
\end{prooftree}$$

$$\begin{prooftree}
\Gamma \prov p \ge q \qquad \Gamma, A[ p] \prov J[p]
\using j L\justifies
\Gamma,  A [\ge q] \prov J[p]
\end{prooftree}$$

\vfil\eject
\noindent Finally here are the rules for $\Diamond$.
$$\begin{prooftree}
\Gamma \prov (A \rposs^{\ge p}) [p]
\using \Diamond R\justifies
\Gamma \prov \Diamond A [p]
\end{prooftree}$$

$$\begin{prooftree}
p \ge q \qquad \Gamma \prov A[p]
\using jR\justifies
\Gamma \prov (A \rrdy^{\ge q}) [p]
\end{prooftree}
\qquad
\begin{prooftree}
p \ge q \qquad \Gamma \prov A[p]
\using jR[T]\justifies
\Gamma \prov (A \rposs^{\ge q}) [p]
\end{prooftree}$$

$$
\begin{prooftree}
\Gamma, \alpha \ge p, A[\alpha] \prov (C \rrdy^{\ge q}) [\alpha]
\using \Diamond L\justifies
\Gamma, \Diamond A[p] \prov (C \rposs^{\ge q}) [p]
\end{prooftree}
\qquad
\begin{prooftree}
\Gamma, \alpha \ge p, A[\alpha] \prov (C \rrdy^{\ge q}) [\alpha]
\using \Diamond L[4]\justifies
\Gamma, \Diamond A[p] \prov (C \rrdy^{\ge q}) [p]
\end{prooftree}
$$

Rules $\Diamond R$, $\Diamond L$, and $jR$ are always included.
$jR[T]$ and $\Diamond L[4]$ are independently optional; including them
 affects the {\em monadic} component of the $\Diamond$ in
ways analogous to the $T$ and $4$ axioms. Specifically I conjecture
that if you appropriately axiomatize $\ge$ and choose the right set of
rules, you get the modal logics in the weather report, like so:

$$\begin{tabular}{l@{\qquad }l@{\qquad }c@{\qquad }c}
Weather Report&$\ge$&$jR[T]$&$\Diamond L[4]$\\
K&no axioms\\
T&refl&\checkmark\\
4&trans&&\checkmark\\
S4&refl, trans&\checkmark&\checkmark\\
\end{tabular}$$

But if one considers these four options for the Kripke relation, and
these four options for the monadic behavior, there's actually a 4 by 4
grid of possibilites. For instance, we can axiomatize $\le$ with no
axioms and include both $jR[T]$ and $\Diamond L[4]$. This gives a logic in
which $\Diamond\Diamond \bot \prov \Diamond \bot$ but not $\Diamond
\Diamond A \prov \Diamond A$. At least this logic is distinct from the
other four above, because with $\Diamond L[4]$ and transitivity of $\ge$, we
can prove $\Diamond \Diamond A \prov \Diamond A$, and without $\Diamond L[4]$,
we cannot prove $\Diamond\Diamond \bot \prov \Diamond \bot$. I would
not tend to guess that all 16 possibilities are distinct.

\subsection{Omitting the Kripke Mechanism}
If we get rid of all mention of explicit worlds, we have four variants of $\Circle$:
$$\begin{prooftree}
\Gamma \prov A \rposs
\using \Circle R\justifies
\Gamma \prov \Circle A
\end{prooftree}$$

$$\begin{prooftree}
  \Gamma \prov A
\using jR\justifies
\Gamma \prov A \rrdy
\end{prooftree}
\qquad
\begin{prooftree}
  \Gamma \prov A
\using jR[T]\justifies
\Gamma \prov A \rposs
\end{prooftree}$$

$$
\begin{prooftree}
\Gamma, A \prov C \rrdy
\using \Circle L\justifies
\Gamma, \Circle A \prov C \rposs
\end{prooftree}
\qquad
\begin{prooftree}
\Gamma,  A \prov C \rrdy
\using \Circle L[4]\justifies
\Gamma, \Circle A \prov C \rrdy
\end{prooftree}
$$
Again, $jR[T]$ and $\Circle L[4]$ are independently optional. The familiar lax logic is the one we get by including both $jR[T]$ and $\Circle L[4]$.

I was hoping that this $\Circle$ with just $T$ and not $4$ might be proof irrelevance, but proof irrelevance would prove $\Circle A \land \Circle B \prov \Circle(A \land B)$
whereas this system does not.
\subsection{Omitting the Monadic Mechanism}
If we get rid of all the monadic tethering, what we get is another $\Diamond$-like operator parametrized over a Kripke relation:
Instead of
$$\begin{tabular}{rl@{\ }l@{\ \ }l}
Right Judgments&$J$&$::=$&$ A \celse (A \rposs^{\ge q}) \celse (A \rrdy^{\ge q})$\\
\end{tabular}
$$
we have simply
$$\begin{tabular}{rl@{\ }l@{\ \ }l}
Right Judgments&$J$&$::=$&$ A \celse  (A \ge q)$\\
\end{tabular}
$$
and rules
$$\begin{prooftree}
\Gamma \prov (A \ge p) [p]
\using \Diamond R\justifies
\Gamma \prov \Diamond A [p]
\end{prooftree}$$

$$\begin{prooftree}
p \ge q \qquad \Gamma \prov A[p]
\using jR\justifies
\Gamma \prov (A  \ge q) [p]
\end{prooftree}
$$

$$
\begin{prooftree}
\Gamma, \alpha \ge p, A[\alpha] \prov J [\alpha]
\using \Diamond L\justifies
\Gamma, \Diamond A[p] \prov J [p]
\end{prooftree}
$$

These systems all prove $\Diamond \bot \prov \bot$ regardless of $\le$, yet
do not prove $\Diamond (A \lor B)\prov \Diamond A \lor \Diamond B$.
\end{document}
