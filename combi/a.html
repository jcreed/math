<head>
<script type="text/javascript"
   src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style>
td {padding: 0px 40px;}
tr.header td {background-color: rgb(225, 225,255);}
</style>
</head>

Say you're trying to do the integral:
\[
\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx + Cx^3}dx
\]
Well... this looks an awful lot like
\[
\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx}dx
\]
which can be solved by more conventional means: you complete the square
\[e^{B^2/4A}\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx - (B^2/4A)}dx\]
\[= e^{B^2/4A}\int\limits_{-\infty}^\infty e^{-(\sqrt Ax +B/2\sqrt A)^2}dx\]
do a substitution \(u = \sqrt Ax +B/2\sqrt A\) to get
\[e^{B^2/4A}\int\limits_{-\infty}^\infty (1/\sqrt{A})e^{-u^2}du
=\sqrt{\frac{\pi}{A}} e^{B^2/4A}\]
(You get \(\int e^{-u^2} du = \sqrt\pi\) via a cute trick that passes through polar coordinates) 
<p>
However, we still have that pesky \(Cx^3\) bit there. What do we do about it?
Let's think like physicists. The little physicist on our shoulder says:
 when in doubt, expand as a power series. Our original integral is equal to:

\[\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx}\left(\sum_{n=0}^\infty \frac{(Cx^3)^n}{n!} \right)\,dx\]
Well, okay, now we've got mere \(x^{3n}\)s where once we had scary \(e^{x^3}\)s.
That feels like progress, although we still can't pull this out of the integral
because it still involves \(x\). Say... hitting that particlar
 exponential expression we have there by \(d/dB\) has the same effect as multiplying by \(x\),
 though. Even if we do it multiple times! That is,
\[ (d/dB)^m (e^{-Ax^2 + Bx}) = x^m (e^{-Ax^2 + Bx})\]
for any m. Therefore


\[\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx}\left(\sum_{n=0}^\infty \frac{(Cx^3)^n}{n!}
 \right)\,dx =
\int\limits_{-\infty}^\infty \sum_{n=0}^\infty\frac{C^n}{n!}(d/dB)^{3n} (e^{-Ax^2 + Bx}) 

\]
and <b>now</b> we can swap the sum and integral and pull all that junk
outside the integral sign, since it's all linear and doesn't depend on
\(x\).
\[=\sum_{n=0}^\infty \frac{C^n}{n!}(d/dB)^{3n}\int\limits_{-\infty}^\infty e^{-Ax^2 + Bx} \]
And we can solve the integral we know how to solve:
\[= \sum_{n=0}^\infty \frac{C^n}{n!}(d/dB)^{3n} \sqrt{\frac{\pi}{A}} e^{B^2/4A}\]
\[=\sqrt{\frac{\pi}{A}} \sum_{n=0}^\infty \frac{C^n}{n!}(d/dB)^{3n} e^{B^2/4A}\]

In some sense, we're done now. We could have Mathematica or whatever
chug away computing the \(3n^{th}\) derivative of \(e^{B^2/4A}\) and
compute this value to whatever accuracy we want, without having to
resort to Riemann sums of the original integral, yuck. But let's look closer at what 
\[ (C^n/n!)(d/dB)^{3n}  e^{B^2/4A}\] really <I>means</I>.
<p>
Better yet, let's just look at
\[ (d/dB)^{m}  e^{B^2/4A}\] 
for the first view values of \(m\).
<center>
<table>
<tr class="header"><td>\(m\)</td><td width=200>\((d/dB)^{m}  e^{B^2/4A}\)</td></tr>
<tr><td>0</td><td>\(e^{B^2/4A}\)</td></tr>
<tr><td>1</td><td>\((B/2A) e^{B^2/4A}\)</td></tr>
<tr><td>2</td><td>\((1/2A + B^2/4A^2) e^{B^2/4A}\)</td></tr>
</table>
</center>
Hmm. Well, obviously every one of these is going to have \(e^{B^2/4A}\) in it, and moreover I'm going
to get sick of writing powers of \(1/2A\). So let's abbreviate \( V = e^{B^2/4A}\) and \(Q = 1/2A\). Then we get
<center>
<table>
<tr class="header"><td>\(m\)</td><td width=200>\((d/dB)^{m}  V\)</td></tr>
<tr><td>0</td><td>\(V\)</td></tr>
<tr><td>1</td><td>\((BQ) V\)</td></tr>
<tr><td>2</td><td>\((Q + B^2Q^2) V\)</td></tr>
<tr><td>3</td><td>\((3BQ^2 + B^3Q^3) V\)</td></tr>
<tr><td>4</td><td>\((3Q^2 + 6B^2Q^3 + B^4Q^4) V\)</td></tr>
<tr><td>...</td><td>...</td></tr>
</table>
</center>
What's the recurrence going on here?
If \(F\) is any old function, and we hit \(FV\) with \(d/dB\), we get
\[(dF/dB)V + F(dV/dB) \]
\[= ((dF/dB) + (BQ)F)V\]
So ignoring the \(V\) that's off to the side, it's like we're repeatedly hitting an expression with the <I>linear operator</I>
\[d/dB + BQ\]
which means "take your function differentiated by \(B\), and take your
original function multiplied by \(BQ\), and add them together." In a table:
<center>
<table>
<tr class="header"><td>\(m\)</td><td width=200>\((d/dB + BQ)^m (1)\)</td></tr>
<tr><td>0</td><td>\(1\)</td></tr>
<tr><td>1</td><td>\(BQ\)</td></tr>
<tr><td>2</td><td>\(Q + B^2Q^2\)</td></tr>
<tr><td>3</td><td>\(3BQ^2 + B^3Q^3\)</td></tr>
<tr><td>4</td><td>\(3Q^2 + 6B^2Q^3 + B^4Q^4\)</td></tr>
<tr><td>...</td><td>...</td></tr>
</table>
</center>
<p>
Now there's a really cool generatingfunctionological story lurking behind all this.
